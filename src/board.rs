use super::{
    Coord,
    Move,
    Moves,
    SideEffect,
    Square,
};

#[derive(Clone, Copy)]
pub struct Board {
    squares: [Square; 64],
}

impl Board {
    pub fn empty() -> Board {
        Board {
            squares: [Square::empty(); 64],
        }
    }

    pub fn default_setup() -> Board {
        let mut board = Board::empty();

        let officers = [
            Square::rook(),
            Square::knight(),
            Square::bishop(),
            Square::queen(),
            Square::king(),
            Square::bishop(),
            Square::knight(),
            Square::rook(),
        ];
        for i in 0..8 {
            board.squares[i] = officers[i];
            board.squares[i + 8] = Square::pawn();
            board.squares[i + 48] = Square::pawn().as_black();
            board.squares[i + 56] = officers[i].as_black();
        }

        board
    }

    pub fn set(&mut self, coord: Coord, square: Square) {
        self.squares[coord.to_index().unwrap()] = square;
    }

    pub fn is_empty(&self, coord: Coord) -> bool {
        if let Some(index) = coord.to_index() {
            self.squares[index].is_empty()
        } else {
            false
        }
    }

    pub fn is_player(&self, coord: Coord, is_black: bool) -> bool {
        if let Some(index) = coord.to_index() {
            if self.squares[index].is_empty() {
                false
            } else {
                self.squares[index].is_black() == is_black
            }
        } else {
            false
        }
    }

    pub fn is_double_step(&self, coord: Coord) -> bool {
        if let Some(index) = coord.to_index() {
            self.squares[index].is_double_step()
        } else {
            false
        }
    }

    pub fn draw(&self) {
        println!("    A B C D E F G H ");
        println!("  +-----------------");

        for j in (0..8).rev() {
            for i in 0..8 {
                let idx = ((j * 8) + i) as usize;
                if i % 8 == 0 {
                    print!("{} |", ((idx / 8) + 1));
                }
                print!(" {}", self.squares[idx]);
                if i % 8 == 7 {
                    println!("");
                    //println!("  | ");
                }
            }
        }
    }

    pub fn get(&self, coord: Coord) -> Option<Square> {
        if let Some(index) = coord.to_index() {
            Some(self.squares[index].clone())
        } else {
            None
        }
    }

    pub fn add_pawn_move(
        &self,
        src: Coord,
        dst: Coord,
        en_passant: bool,
        moves: &mut Moves
    ) {
        if dst.y() == 0 || dst.y() == 7 {
            let side_effects = [
                SideEffect::PromoteToKnight,
                SideEffect::PromoteToRook,
                SideEffect::PromoteToBishop,
                SideEffect::PromoteToQueen,
            ];

            for side_effect in &side_effects {
                moves.push(
                    Move {
                        src,
                        dst,
                        side_effect: Some(*side_effect),
                    }
                );
            }
        } else {
            moves.push(
                Move {
                    src,
                    dst: dst,
                    side_effect: if en_passant {
                        Some(SideEffect::DoublePawnStep)
                    } else {
                        None
                    },
                }
            );
        }

    }

    pub fn get_possible_moves(&self, is_black: bool) -> Moves {
        let mut moves = Moves::new();
        let mut square_index = 0;
        let forward = Coord::new(0, if is_black { -1 } else { 1 });
        for j in 0..8 {
            for i in 0..8 {
                let src = Coord::new(i, j);
                let square = self.squares[square_index];
                square_index += 1;

                if square.is_black() != is_black {
                    continue;
                }

                if square.is_empty() {
                    continue;
                }

                if square.is_pawn() {
                    // One step forwad
                    let dst = src + forward;
                    if self.is_empty(dst) {
                        self.add_pawn_move(src, dst, false, &mut moves);
                    }

                    // Double step forward
                    let dst = src + forward + forward;
                    let valid =
                        !square.has_moved() &&
                        self.is_empty(dst) &&
                        self.is_empty(dst - forward);

                    if valid {
                        self.add_pawn_move(src, dst, true, &mut moves);
                    }

                    let sides = [
                        Coord::new(1, 0),
                        Coord::new(-1, 0)
                    ];

                    for side in &sides {
                        // Regular diagonal capture
                        let dst = src + forward + *side;
                        if self.is_player(dst, !is_black) {
                            self.add_pawn_move(src, dst, false, &mut moves);
                        }

                        // En passant capture
                        let dst = src + forward + *side;
                        let target = src + *side;

                        let valid =
                            self.is_empty(dst) &&
                            self.is_player(target, !is_black) &&
                            self.is_double_step(target);

                        if valid {
                            let capture =
                                SideEffect::Capture(target);

                            moves.push(
                                Move {
                                    src,
                                    dst,
                                    side_effect: Some(capture),
                                }
                            );
                        }
                    }

                } else if square.is_rook() {
                    let directions = [
                        Coord::new(1, 0),
                        Coord::new(-1, 0),
                        Coord::new(0, 1),
                        Coord::new(0, -1),
                    ];

                    self.add_directional_moves(
                        src,
                        &directions,
                        8,
                        is_black,
                        &mut moves,
                    );
                } else if square.is_bishop() {
                    let directions = [
                        Coord::new(1, 1),
                        Coord::new(-1, 1),
                        Coord::new(1, -1),
                        Coord::new(-1, -1),
                    ];

                    self.add_directional_moves(
                        src,
                        &directions,
                        8,
                        is_black,
                        &mut moves,
                    );
                } else if square.is_queen() {
                    let directions = [
                        Coord::new(1, 1),
                        Coord::new(-1, 1),
                        Coord::new(1, -1),
                        Coord::new(-1, -1),
                        Coord::new(1, 0),
                        Coord::new(-1, 0),
                        Coord::new(0, 1),
                        Coord::new(0, -1),
                    ];

                    self.add_directional_moves(
                        src,
                        &directions,
                        8,
                        is_black,
                        &mut moves,
                    );
                } else if square.is_king() {
                    let directions = [
                        Coord::new(1, 1),
                        Coord::new(-1, 1),
                        Coord::new(1, -1),
                        Coord::new(-1, -1),
                        Coord::new(1, 0),
                        Coord::new(-1, 0),
                        Coord::new(0, 1),
                        Coord::new(0, -1),
                    ];

                    self.add_directional_moves(
                        src,
                        &directions,
                        1,
                        is_black,
                        &mut moves,
                    );

                    self.add_castle_moves(src, is_black, &mut moves);
                } else if square.is_knight() {
                    let directions = [
                        Coord::new(2, 1),
                        Coord::new(2, -1),
                        Coord::new(-2, 1),
                        Coord::new(-2, -1),
                        Coord::new(1, 2),
                        Coord::new(-1, 2),
                        Coord::new(1, -2),
                        Coord::new(-1, -2,),
                    ];

                    self.add_directional_moves(
                        src,
                        &directions,
                        1,
                        is_black,
                        &mut moves,
                    );
                }
            }
        }

        moves
    }

    pub fn add_castle_moves(
        &self,
        src: Coord,
        is_black: bool,
        moves: &mut Moves,
    ) {
        let index =
            if let Some(index) = src.to_index() {
                index
            } else {
                return;
            };

        let square = self.squares[index];

        if !square.is_king() {
            return;
        }

        if square.has_moved() {
            return;
        }

        if square.is_black() != is_black {
            return;
        }

        let directions = [
            Coord::new(-1, 0),
            Coord::new(1, 0)
        ];

        for direction in &directions {
            let mut rook_src = src;
            loop {
                rook_src = rook_src + *direction;

                let square =
                    if let Some(square) = self.get(rook_src) {
                        square
                    } else {
                        break;
                    };

                if square.is_rook() {
                    if !(square.is_black() == is_black) {
                        break;
                    }

                    if !square.has_moved() {
                        let dst =
                            src + *direction + *direction;

                        let rook_dst = dst - *direction;

                        let side_effect =
                            SideEffect::Castle(rook_src, rook_dst);

                        let main_move =
                            Move {
                                src,
                                dst,
                                side_effect: Some(side_effect),
                            };

                        moves.push(main_move);
                    }
                }

                // TODO: Check if square is attacked by other
                // player

                if !square.is_empty() {
                    break;
                }
            }
        }
    }


    pub fn add_directional_moves(
        &self,
        src: Coord,
        directions: &[Coord],
        max_count: usize,
        is_black: bool,
        moves: &mut Moves,
    ) {
        for direction in directions {
            let mut dst = src;
            for _ in 0..max_count {
                dst = dst + *direction;
                if self.is_empty(dst) {
                    let m = Move { src, dst, side_effect: None };
                    moves.push(m);
                } else {
                    if self.is_player(dst, !is_black) {
                        let m = Move { src, dst, side_effect: None };
                        moves.push(m);
                    }
                    break;
                }
            }
        }
    }

    pub fn check_winner(&self) -> Option<bool> {
        let mut found_white_king = false;
        let mut found_black_king = false;
        for square in &self.squares {
            if square.is_king() {
                found_white_king |= !square.is_black();
                found_black_king |= square.is_black();
            }
        }
        if !found_white_king {
            return Some(true);
        }
        if !found_black_king {
            return Some(false);
        }
        return None;
    }

    pub fn perform_move(&mut self, m: Move) {
        if m.is_dummy() {
            return;
        }

        // Double step pawn flag is used to detect the possibility
        // of en passant. This is only valid for the very next move.
        for i in 0..64 {
            self.squares[i].clear_double_step();
        }

        let dst_index = m.dst.to_index().expect("Bad move");
        let src_index = m.src.to_index().expect("Bad move");

        self.squares[dst_index] = self.squares[src_index];
        self.squares[dst_index].mark_moved();
        self.squares[src_index] = Square::empty();

        if let Some(side_effect) = m.side_effect {
            match side_effect {
                SideEffect::PromoteToKnight => {
                    self.squares[dst_index].promote(Square::knight());
                },
                SideEffect::PromoteToRook => {
                    self.squares[dst_index].promote(Square::rook());
                },
                SideEffect::PromoteToBishop => {
                    self.squares[dst_index].promote(Square::bishop());
                },
                SideEffect::PromoteToQueen => {
                    self.squares[dst_index].promote(Square::queen());
                },
                SideEffect::Castle(src, dst) => {
                    self.perform_move(
                        Move { src, dst, side_effect: None }
                    );
                },
                SideEffect::DoublePawnStep => {
                    self.squares[dst_index].mark_double_step();
                },
                SideEffect::Capture(coord) => {
                    self.squares[coord.to_index().unwrap()] =
                        Square::empty();
                },
                //_ => { todo!() }
            }
        }
    }

    pub fn calculate_score(&self) -> i32 {
        let mut total_score = 0;
        for square in &self.squares {
            total_score += square.value() * 10;
        }

        // One point for each of the center squares occupied
        for i in 3..5 {
            for j in 3..5 {
                let idx = Coord::new(i, j).to_index().unwrap();
                if !self.squares[idx].is_empty() {
                    if self.squares[idx].is_black() {
                        total_score -= 1;
                    } else {
                        total_score += 1;
                    }
                }

            }
        }

        total_score
    }
}

