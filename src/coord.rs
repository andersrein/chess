#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct Coord {
    x: i8,
    y: i8,
}

impl Coord {
    pub const fn new(x: i8, y: i8) -> Coord { Coord { x, y } }

    pub fn to_index(&self) -> Option<usize> {
        if self.x < 0 || self.x >= 8 || self.y < 0 || self.y >= 8 {
            None
        } else {
            Some(((self.y * 8) + self.x) as usize)
        }
    }

    pub fn x(&self) -> i8 { self.x }
    pub fn y(&self) -> i8 { self.y }
}

impl std::ops::Add for Coord {
    type Output = Coord;
    fn add(self, other: Coord) -> Coord {
        Coord {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl std::ops::Sub for Coord {
    type Output = Coord;
    fn sub(self, other: Coord) -> Coord {
        Coord {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl std::fmt::Display for Coord {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}{}", (97u8 + self.x as u8) as char, self.y + 1)
    }
}

#[derive(Debug)]
pub struct ParseCoordError {
    string: String,
}

impl std::str::FromStr for Coord {
    type Err = ParseCoordError;

    fn from_str(s: &str) -> Result<Coord, Self::Err> {
        let bytes = s.as_bytes();

        let invalid =
            (bytes.len() != 2) ||
            (bytes[0] < 97) || (bytes[0] >= 97 + 8) ||
            (bytes[1] < 48) || (bytes[1] >= 48 + 10);

        if invalid {
            return Err(ParseCoordError { string: s.to_string() });
        }

        let x = (bytes[0] as i8) - 97;
        let y = ((bytes[1] as i8) - 48) - 1;

        Ok(Coord::new(x, y))
    }
}


