use super::Coord;

const MAX_MOVES : usize = 256;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum SideEffect {
    PromoteToKnight,
    PromoteToRook,
    PromoteToBishop,
    PromoteToQueen,
    DoublePawnStep,
    Capture(Coord),
    Castle(Coord, Coord),
}

#[derive(Debug)]
pub struct ParseError;

impl std::str::FromStr for SideEffect {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<SideEffect, Self::Err> {
        if s == "N" {
            Ok(SideEffect::PromoteToKnight)
        } else if s == "R" {
            Ok(SideEffect::PromoteToRook)
        } else if s == "B" {
            Ok(SideEffect::PromoteToBishop)
        } else if s == "Q" {
            Ok(SideEffect::PromoteToQueen)
        } else {
            Err(ParseError)
        }
    }
}

const DUMMY_COORD : Coord = Coord::new(127, 127);

#[derive(Debug, Copy, Clone)]
pub struct Move {
    pub src: Coord,
    pub dst: Coord,
    pub side_effect: Option<SideEffect>,
}

impl Move {
    pub fn dummy() -> Self {
        Move {
            src: DUMMY_COORD,
            dst: DUMMY_COORD,
            side_effect: None,
        }
    }

    pub fn is_dummy(&self) -> bool {
        (self.src == DUMMY_COORD) || (self.dst == DUMMY_COORD)
    }
}

impl std::fmt::Display for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}{}", self.src, self.dst)?;
        match self.side_effect {
            Some(SideEffect::Castle(_, _)) => write!(f, "C")?,
            Some(SideEffect::DoublePawnStep) => { },
            Some(SideEffect::PromoteToKnight) => write!(f, "N")?,
            Some(SideEffect::PromoteToBishop) => write!(f, "B")?,
            Some(SideEffect::PromoteToRook) => write!(f, "R")?,
            Some(SideEffect::PromoteToQueen) => write!(f, "Q")?,
            _ => { },
        }
        Ok(())
    }
}

#[derive(Copy, Clone)]
pub struct Moves {
    moves: [Move; MAX_MOVES],
    count: usize,
}

impl Moves {
    pub fn new() -> Moves {
        Moves {
            moves: [Move::dummy(); MAX_MOVES],
            count: 0,
        }
    }

    pub fn push(&mut self, m: Move) {
        self.moves[self.count] = m;
        self.count += 1;
    }
}

#[derive(Copy, Clone)]
pub struct MoveRandomizer {
    seed: u32,
}

impl MoveRandomizer {
    pub fn new() -> Self {
        use std::time::{SystemTime, UNIX_EPOCH};

        let start = SystemTime::now();
        let seconds_since_epoch = start
            .duration_since(UNIX_EPOCH)
            .expect("Time went backwards")
            .as_millis();

        MoveRandomizer {
            seed: (seconds_since_epoch % std::u32::MAX as u128) as u32
        }
    }

    fn random(&mut self) -> u32 {
        let new_seed =
            (self.seed as u64 * 193939u64) % std::u32::MAX as u64;

        self.seed = new_seed as u32;
        self.seed % 1000
    }

    pub fn randomize_moves(&mut self, moves: &mut Moves) {
        if moves.count == 0 {
            return;
        }
        let tmp = *moves;
        let offset = (self.random() % tmp.count as u32) as usize;
        for i in 0..tmp.count {
            moves.moves[i] = tmp.moves[(i + offset) % tmp.count];
        }
    }
}

pub struct MoveIterator {
    moves: Moves,
    index: usize,
}

impl MoveIterator {
    pub fn get_progress(&self, range: usize) -> usize {
        (self.index * range) / self.moves.count
    }
}

impl Iterator for MoveIterator {
    type Item = Move;
    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.moves.count {
            None
        } else {
            let m = self.moves.moves[self.index].clone();
            self.index += 1;
            Some(m)
        }
    }
}

impl IntoIterator for Moves {
    type Item = Move;
    type IntoIter = MoveIterator;

    fn into_iter(self) -> MoveIterator {
        MoveIterator {
            moves: self,
            index: 0,
        }
    }
}

impl std::fmt::Display for Moves {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut m = std::collections::BTreeMap::new();
        for i in 0..self.count {
            m.entry(self.moves[i].src)
                .or_insert_with(|| Vec::new())
                .push(self.moves[i].clone());
        }

        for (_, dsts) in m {
            write!(f, "  ")?;
            let mut first = true;
            for dst in dsts {
                if !first { write!(f, ", ")?; }
                write!(f, "{}", dst)?;
                first = false;
            }
            writeln!(f, "")?;
        }

        Ok(())
    }
}

pub struct MoveSpec {
    src: Option<Coord>,
    dst: Coord,
    side_effect: Option<SideEffect>,
}

impl MoveSpec {
    pub fn matches(&self, m: &Move) -> bool {
        if let Some(src) = self.src {
            if src != m.src {
                return false;
            }
        }

        if let Some(side_effect) = self.side_effect {
            if let Some(move_side_effect) = m.side_effect {
                if side_effect != move_side_effect {
                    return false;
                }
            }
        }

        self.dst == m.dst
    }
}

impl std::str::FromStr for MoveSpec {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<MoveSpec, Self::Err> {
        if s.len() < 2 || s.len() > 5 {
            return Err(ParseError);
        }

        let first = s[0..2].parse()
            .map_err(|_| ParseError)?;

        let mut move_spec =
            MoveSpec {
                src: None,
                dst: first,
                side_effect: None,
            };

        let mut i = 2;
        if s.len() == i + 2 {
            move_spec.src = Some(move_spec.dst);
            move_spec.dst = s[i..i + 2].parse()
                .map_err(|_| ParseError)?;
            i += 2;
        }

        if s.len() == i + 1 {
            let side_effect = s[i..i + 1].parse()
                .map_err(|_| ParseError)?;
            move_spec.side_effect = Some(side_effect);
        }

        Ok(move_spec)
    }
}

