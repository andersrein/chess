
pub trait ZeroSum {
    type PlayerIndex;

    type State : Clone + Copy;
    type Scores;
    type Move;
    type MoveIterator : Iterator<Item=Self::Move>;

    fn dummy_move() -> Self::Move;

    fn next_player(player_index: &Self::PlayerIndex)
        -> Self::PlayerIndex;

    fn get_moves(
        &mut self,
        state: &Self::State,
        player_index: &Self::PlayerIndex,
    ) -> Self::MoveIterator;

    fn apply_move(
        &mut self,
        state: &mut Self::State,
        m: &Self::Move
    );

    fn get_scores(&mut self, state: &Self::State)
        -> Self::Scores;

    fn get_score_for_player(
        scores: &Self::Scores,
        player_index: &Self::PlayerIndex
    ) -> i32;

    fn report_progress(iterator: &Self::MoveIterator);
}


pub struct AI<T: ZeroSum> {
    moves_evaluated: usize,
    search_depth: usize,
    zero_sum: T,
    time_spent: std::time::Duration,
    _marker: std::marker::PhantomData<T>,
}
impl<T: ZeroSum> AI<T> {
    pub fn new(zero_sum: T) -> AI<T> {
        AI {
            moves_evaluated: 0,
            search_depth: 4,
            zero_sum,
            time_spent: std::time::Duration::from_millis(0),
            _marker: std::marker::PhantomData,
        }
    }

    pub fn set_search_depth(&mut self, depth: usize) {
        self.search_depth = depth;
    }

    pub fn moves_evaluated(&self) -> usize {
        self.moves_evaluated
    }

    pub fn time_spent(&self) -> std::time::Duration {
        self.time_spent
    }

    pub fn moves_evaluated_per_second(&self) -> usize {
        let secs = self.time_spent.as_secs_f64();
        let moves = self.moves_evaluated as f64;
        (moves / secs) as usize
    }

    fn best_move_recursive(
        &mut self,
        state: &T::State,
        player_index: T::PlayerIndex,
        depth: usize,
        report_progress: bool,
    ) -> (T::Scores, T::Move) {
        let mut best : Option<(T::Scores, T::Move)> = None;

        let mut it =
            self.zero_sum.get_moves(&state, &player_index)
                .into_iter();

        if report_progress {
            T::report_progress(&it);
        }
        while let Some(m) = it.next() {
            let mut new_state = *state;
            self.zero_sum.apply_move(&mut new_state, &m);

            self.moves_evaluated += 1;

            let scores =
                if depth == 0 {
                    self.zero_sum.get_scores(&new_state)
                } else {
                    let (scores, _) =
                        self.best_move_recursive(
                            &new_state,
                            T::next_player(&player_index),
                            depth - 1,
                            false,
                        );
                    scores
                };

            if let Some((best_scores, best_move)) = &mut best {
                let best_player_score =
                    T::get_score_for_player(
                        best_scores,
                        &player_index
                    );

                let player_score =
                    T::get_score_for_player(&scores, &player_index);

                if best_player_score < player_score {
                    *best_scores = scores;
                    *best_move = m;
                }
            } else {
                best = Some((scores, m));
            }

            if report_progress {
                T::report_progress(&it);
            }
        }

        // If there is no moves for this player we just return a dummy
        // move. It doesn't really matter what the move is because
        // it must mean that all the players pieces have been taken
        // and that should be reflected in the calculated score
        best.unwrap_or(
            (self.zero_sum.get_scores(state), T::dummy_move())
        )
    }

    pub fn find_best_move(
        &mut self,
        state: T::State,
        player_index: T::PlayerIndex,
    ) -> T::Move {
        let start = std::time::Instant::now();

        let (_, best_move) =
            self.best_move_recursive(
                &state,
                player_index,
                self.search_depth,
                true,
            );

        let end = std::time::Instant::now();

        self.time_spent += end - start;

        best_move
    }
}
