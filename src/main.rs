mod square;
pub use square::Square;

mod coord;
pub use coord::Coord;

mod moves;
pub use moves::{
    SideEffect,
    Move,
    Moves,
    MoveRandomizer,
    MoveSpec,
    MoveIterator,
};

mod board;
pub use board::{
    Board,
};

mod zero_sum;

struct ChessAI {
    move_randomizer: MoveRandomizer,
}

impl ChessAI {
    pub fn new() -> ChessAI {
        ChessAI {
            move_randomizer: MoveRandomizer::new(),
        }
    }
}

impl zero_sum::ZeroSum for ChessAI {
    // The only player index is a boolean which means "is_black"
    type PlayerIndex = bool;

    // The since the score for the black player is just the inverse
    // of the white players score we only need one single integer
    // to keep the score for both players
    type Scores = i32;

    type State = Board;

    type Move = Move;
    type MoveIterator = MoveIterator;

    fn dummy_move() -> Move { Move::dummy() }

    fn next_player(player_index: &bool) -> bool {
        !*player_index
    }

    fn get_moves(
        &mut self,
        board: &Board,
        player_index: &bool
    ) -> MoveIterator {
        let mut moves = board.get_possible_moves(*player_index);
        self.move_randomizer.randomize_moves(&mut moves);
        moves.into_iter()
    }

    fn apply_move(&mut self, board: &mut Board, m: &Self::Move) {
        board.perform_move(*m);
    }

    fn get_scores(&mut self, board: &Board) -> i32 {
        board.calculate_score()
    }

    fn get_score_for_player(
        scores: &i32,
        is_blacks_turn: &bool
    ) -> i32 {
        if *is_blacks_turn {
            -*scores
        } else {
            *scores
        }
    }

    fn report_progress(iterator: &MoveIterator) {
        const PROGRESS_WIDTH : usize = 80;

        let progress = iterator.get_progress(PROGRESS_WIDTH);

        print!("[");
        for _ in 0..progress {
            print!("=");
        }
        for _ in progress..PROGRESS_WIDTH {
            print!(" ");
        }
        print!("]\r");

        if progress == PROGRESS_WIDTH {
            println!();
        }

        use std::io::Write;
        std::io::stdout().flush().unwrap();
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum PlayerType {
    Human,
    AI,
}

impl std::str::FromStr for PlayerType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.to_lowercase() == "ai" {
            Ok(PlayerType::AI)
        } else if s.to_lowercase() == "human" {
            Ok(PlayerType::Human)
        } else {
            Err(format!("Invalid player type string: {:?}", s))
        }
    }
}

fn main() {
    let matches =
        clap::App::new("ChessAI")
            .version("0.1")
            .author("Anders Rein <andersrein87@gmail.com>")
            .about("Very slow and stupid Chess AI")
            .arg(clap::Arg::with_name("white")
                .short("w")
                .long("white")
                .value_name("PLAYER_TYPE")
                .help("Choose human or AI for white")
                .takes_value(true)
            )
            .arg(clap::Arg::with_name("black")
                .short("b")
                .long("black")
                .value_name("PLAYER_TYPE")
                .help("Choose human or AI for black")
                .takes_value(true)
            )
            .get_matches();

    let white_type : PlayerType =
        matches.value_of("white")
            .unwrap_or("human")
            .to_lowercase()
            .parse()
            .expect("Invalid value for white");

    let black_type : PlayerType =
        matches.value_of("black")
            .unwrap_or("ai")
            .parse()
            .expect("Invalid value for white");

    use std::io::BufRead;
    let stdin = std::io::stdin();
    let stdin_lock = stdin.lock();
    let mut stdin_lines = stdin_lock.lines();

    let mut board = Board::default_setup();

    let mut is_blacks_turn = false;

    let mut ai = zero_sum::AI::new(ChessAI::new());

    loop {
        board.draw();

        if let Some(winner_is_black) = board.check_winner() {
            if winner_is_black {
                println!("\nBlack is the Winner!");
            } else {
                println!("\nWhite is the Winner!");
            }
            return;
        }

        println!("\nScore: {}", board.calculate_score());

        let is_ai =
            (is_blacks_turn && black_type == PlayerType::AI) ||
            (!is_blacks_turn && white_type == PlayerType::AI);

        if is_ai {
            println!("Computer is working... \n\n");

            ai.set_search_depth(4);

            let m = ai.find_best_move(board, is_blacks_turn);

            println!(
                "\nEvaluated {} moves in {}ms ({} moves per second).",
                ai.moves_evaluated(),
                ai.time_spent().as_millis(),
                ai.moves_evaluated_per_second(),
            );

            board.perform_move(m);
            println!("\nComputer chose move: {}\n", m);
        } else {
            let moves = board.get_possible_moves(is_blacks_turn);

            println!("\nPossible moves:\n{}", moves);

            print!("Enter move:");
            use std::io::Write;
            std::io::stdout().flush().unwrap();

            let line = stdin_lines.next().unwrap().unwrap();

            let input_move : MoveSpec =
                match line.parse() {
                    Ok(input_move) => { input_move },
                    Err(error) => {
                        println!("ERROR: {:?}", error);
                        continue;
                    },
                };

            println!("");

            let mut selected_moves = Vec::new();

            for m in moves {
                if input_move.matches(&m) {
                    selected_moves.push(m.clone());
                }
            }

            if selected_moves.len() == 0 {
                println!("ERROR: Illegal move!");
                continue;
            }

            if selected_moves.len() > 1 {
                println!("ERROR: Ambiguous move specificiation");
                continue;
            }

            let selected_move =
                selected_moves.into_iter().next().unwrap();

            board.perform_move(selected_move);
        }

        is_blacks_turn = !is_blacks_turn;

    }
}
