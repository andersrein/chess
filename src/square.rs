const PAWN : u8 = 0x01;
const ROOK : u8 = 0x02;
const KNIGHT : u8 = 0x03;
const BISHOP : u8 = 0x04;
const QUEEN : u8 = 0x05;
const KING : u8 = 0x06;

const PIECE_MASK : u8 = 0x07;
const IS_BLACK : u8 = 0x08;
const HAS_MOVED : u8 = 0x10;
const EN_PASSANT : u8 = 0x20;

#[derive(Clone, Copy, Debug)]
pub struct Square {
    value: u8,
}

impl Square {
    pub fn empty() -> Self { Self { value: 0u8 } }

    // Piece constructors
    pub fn pawn() -> Self { Self { value: PAWN } }
    pub fn rook() -> Self { Self { value: ROOK } }
    pub fn knight() -> Self { Self { value: KNIGHT } }
    pub fn bishop() -> Self { Self { value: BISHOP } }
    pub fn queen() -> Self { Self { value: QUEEN } }
    pub fn king() -> Self { Self { value: KING } }

    pub fn as_black(self) -> Self {
        Self { value: self.value | IS_BLACK }
    }

    pub fn mark_moved(&mut self) {
        self.value |= HAS_MOVED;
    }

    pub fn mark_double_step(&mut self) {
        self.value |= EN_PASSANT;
    }

    pub fn clear_double_step(&mut self) {
        self.value &= !EN_PASSANT;
    }

    pub fn is_double_step(&self) -> bool {
        self.value & EN_PASSANT != 0
    }

    pub fn promote(&mut self, square: Square) {
        let piece = square.piece();
        self.value = (self.value & !PIECE_MASK) | piece;
    }

    fn piece(&self) -> u8 { self.value & PIECE_MASK }

    pub fn is_black(&self) -> bool { self.value & IS_BLACK != 0 }
    pub fn is_empty(&self) -> bool { self.value == 0 }
    pub fn is_pawn(&self) -> bool { self.piece() == PAWN }
    pub fn is_rook(&self) -> bool { self.piece() == ROOK }
    pub fn is_knight(&self) -> bool { self.piece() == KNIGHT }
    pub fn is_bishop(&self) -> bool { self.piece() == BISHOP }
    pub fn is_queen(&self) -> bool { self.piece() == QUEEN }
    pub fn is_king(&self) -> bool { self.piece() == KING }
    pub fn has_moved(&self) -> bool { self.value & HAS_MOVED != 0 }

    pub fn value(&self) -> i32 {
        let piece = self.piece();
        if piece == 0 {
            return 0;
        };

        let mut score =
            match piece {
                PAWN => 1,
                KNIGHT => 3,
                BISHOP => 3,
                ROOK => 5,
                QUEEN => 9,
                KING => 1000000,
                _ => {
                    panic!("Invalid value")
                }
            };

        if self.is_black() {
            score = -score;
        }

        score
    }
}

impl Into<Square> for u8 {
    fn into(self) -> Square {
        Square { value: self }
    }
}

impl std::fmt::Display for Square {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let piece = self.piece();
        if piece == 0 {
            write!(f, "\u{2022}")
        } else {
            use termion::*;
            if self.is_black() {
                write!(f, "{}", color::Fg(color::Red))?;
            } else {
                write!(f, "{}", color::Fg(color::Blue))?;
            }
            match piece {
                PAWN => { write!(f, "\u{265F}") },
                ROOK => { write!(f, "\u{265C}") },
                KNIGHT => { write!(f, "\u{265E}") },
                BISHOP => { write!(f, "\u{265D}") },
                KING => { write!(f, "\u{265B}") },
                QUEEN => { write!(f, "\u{265A}") },
                _ => {
                    panic!("Invalid piece value");
                }
            }?;

            write!(f, "{}", color::Fg(color::Reset))
        }
    }
}

